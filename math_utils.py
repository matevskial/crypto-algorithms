import numpy as np

def extended_euclid_gcd(a, b):
    """
    Returns a tuple `result` of size 3 where:
    Referring to the equation ax + by = gcd(a, b)
        result[0] is gcd(a, b)
        result[1] is x
        result[2] is y 
    """
    s = 0; old_s = 1
    t = 1; old_t = 0
    r = b; old_r = a

    while r != 0:
        quotient = old_r//r # // operator performs integer or floored division
        old_r, r = r, old_r - quotient*r
        old_s, s = s, old_s - quotient*s
        old_t, t = t, old_t - quotient*t

    return (old_r, old_s, old_t)

def inverse_matrix_by_mod(mat, mod):
    """
    Returns inverse numpy matrix by module
    """

    d = np.int64(np.linalg.det(mat).round())
    g, e, t = extended_euclid_gcd(d, 26)

    inv_mat = np.linalg.inv(mat) * d * e
    inv_mat = inv_mat.round().astype(np.int64) % 26

    return inv_mat
