from string import ascii_uppercase

import numpy as np

import math_utils

"""
Module that contains implementations of some Ciphers

Each implementation of the ciphers is encapsulated in a class that contains methods
for encryption and decryption
"""

class CaesarCipher(object):

    def encrypt(self, p, k):
        res = ''
        for x in filter(lambda x: x != ' ', p.upper()):
            ordinal = ord(x) - ord('A')
            ordinalCiphered = (ordinal + k) % 26

            res += chr(ordinalCiphered + ord('A'))

        return res

    def decrypt(self, p, k):
        res = ''
        for x in filter(lambda x: x != ' ', p.upper()):
            ordinal = ord(x) - ord('A')
            ordinalDeciphered = (ordinal - k + 26) % 26

            res += chr(ordinalDeciphered + ord('A'))

        return res

class AffineCipher(object):

    def __init__(self):
        self.inverse = {1: 1, 3: 9, 5:21, 7:15, 9:3, 11:19, 15:7, 17:23, 19:11, 21:5, 23:17, 25:25}

    def encrypt(self, p, k):
        a, b = k
        if a not in self.inverse:
            raise Exception("a has no inverse in ring Z26")
        res = ''
        for x in filter(lambda x: x != ' ', p.upper()):
            ordinal = ord(x) - ord('A')
            ordinalCiphered = (a * ordinal + b) % 26
            res += chr(ordinalCiphered + ord('A'))

        return res

    def decrypt(self, p, k):
        a, b = k
        if a not in self.inverse:
            raise Exception("a has no inverse in ring Z26")
        res = ''
        
        inverse_a = self.inverse[a]

        for x in filter(lambda x: x != ' ', p.upper()):
            ordinal = ord(x) - ord('A')
            ordinalDeciphered = (inverse_a * (ordinal - b) + 26) % 26 
            res += chr(ordinalDeciphered + ord('A'))

        return res

class PlayfairCipher(object):
    
    def __build_mat(self, k):
        mat = {}
        
        i, j = 0, 0

        for c in (k.upper() + ascii_uppercase):
            if j == 5:
                i += 1
                j = 0

            if c == 'J':
                c = 'I'

            if c not in mat:
                mat[c] = (i, j)
                mat[(i, j)] = c
                j += 1

        return mat

    def __get_bigrams(self, p):
        
        bigrams = []
        i = 0
        while i < len(p):
            if i == len(p) - 1:
                bigrams.append(p[i] + 'X')
                i += 1
                continue
            
            first = "I" if p[i] == "J" else p[i]
            second = "I" if p[i + 1] == "J" else p[i + 1]

            if first == second:
                bigrams.append(first + 'X')
                i += 1
            else:
                bigrams.append(first + second)
                i += 2

        return bigrams

    def encrypt(self, p, k):
        return self.__crypt(p, k, self.__encrypt_bigram)
    
    def decrypt(self, p, k):
        return self.__crypt(p, k, self.__decrypt_bigram)

    def __crypt(self, p, k, crypt_fun):
        '''
        A general method that encapsulates the process of encryption or decryption

        crypt_fun detemines if this is encryption or decryption
        '''

        mat = self.__build_mat(k)
        bigrams = self.__get_bigrams(p)
        res = ''

        for b in bigrams:
            res += crypt_fun(b, mat)

        return res

    def __encrypt_bigram(self, bigram, mat):
        first, second = bigram
        
        first_pos, second_pos = mat[first], mat[second]

        first_res, second_res = (), ()
        if first_pos[0] == second_pos[0]:
            first_res = (first_pos[0], (first_pos[1] + 1) % 5)
            second_res = (second_pos[0], (second_pos[1] + 1) % 5)
        elif first_pos[1] == second_pos[1]:
            first_res = ( (first_pos[0] + 1) % 5, first_pos[1])
            second_res = ((second_pos[0] + 1) % 5, second_pos[1])
        else:
            first_res = (first_pos[0], second_pos[1])
            second_res = (second_pos[0], first_pos[1])

        return mat[first_res] + mat[second_res]

    def __decrypt_bigram(self, bigram, mat):
        first, second = bigram
        
        first_pos, second_pos = mat[first], mat[second]

        first_res, second_res = (), ()

        if first_pos[0] == second_pos[0]:
            first_res = (first_pos[0], (first_pos[1] - 1 + 5) % 5)
            second_res = (second_pos[0], (second_pos[1] - 1 + 5) % 5)
        elif first_pos[1] == second_pos[1]:
            first_res = ( (first_pos[0] - 1 + 5) % 5, first_pos[1])
            second_res = ((second_pos[0] - 1 + 5) % 5, second_pos[1])
        else:
            first_res = (first_pos[0], second_pos[1])
            second_res = (second_pos[0], first_pos[1])

        return mat[first_res] + mat[second_res]

class VigenereCipher(object):

    def __init__(self, auto_key = False):
        self.auto_key = auto_key

    def encrypt(self, p, k):
        return self.__crypt(p, k, self.__encrypt_character)

    def decrypt(self, p, k):
        return self.__crypt(p, k, self.__decrypt_character)

    def __crypt(self, p, k, crypt_fun):
        '''
        A general method that encapsulates the process of encryption or decryption

        crypt_fun detemines if this is encryption or decryption
        '''
        i = 0
        res = ''
        k_list = list(k)

        for e in p:
            ordinalP = ord(e) - ord('A')
            ordinalK = ord(k_list[i]) - ord('A')

            res += crypt_fun(ordinalP, ordinalK, k_list, i)

            i = (i + 1) % len(k)

        return res

    def __encrypt_character(self, p, k, k_list, i):
        res = chr((p + k) % 26 + ord('A'))

        if self.auto_key:
            k_list[i] = chr(p + ord('A'))

        return res

    def __decrypt_character(self, p, k, k_list, i):
        res = chr( (p - k + 26) % 26 + ord('A') )
        if self.auto_key:
            k_list[i] = res

        return res

class HillCipher(object):
    
    def encrypt(self, p, k):
        return self.__crypt(p, k)

    def decrypt(self, p, k):
        return self.__crypt(p, k, decryption=True)

    def __crypt(self, p, k, decryption=False):
        m = len(k)
        p += 'X' * (len(p) % m) # pad plaintext so every block of p has size m

        k_mat = np.array(k)
        
        if decryption:
            k_mat = math_utils.inverse_matrix_by_mod(k_mat, 26)

        windows = len(p) // m
        res = ''

        for i in range(windows):
            leftInc = i*m
            rightExcl = leftInc + m
            # w will be list of list, that represents a matrix with m rows and 1 column
            w = list(map(lambda x: [ord(x) - ord('A')], p[leftInc : rightExcl]))
            
            cipheredMat = np.matmul(k_mat, np.array(w)) % 26

            for e in cipheredMat.flat:
                res += chr(e + ord('A'))

        return res
