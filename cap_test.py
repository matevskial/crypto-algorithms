import unittest
import cap

"""
Module that contains tests for the ciphers

The general structure of CRYPT_DATA:
    each key of CRYPT_DATA represents a tuple that contains the plaintext and ciphertext
    each value of CRYPT_DATA represents the key used for encryption/decryption

    for CRYPT_DATA in class PlayfairCipherTest, the key represents a tuple that contains
    the plaintext, the modified version of the plaintext(because when encrypting, playfair modifies the plaintext before encryption), and the ciphertext
"""

class PlayfairCipherTest(unittest.TestCase):
    
    CRYPT_DATA = {
            ('KRIPTOGRAFIJAIKRIPTOZASHTITA', 'KRIPTOGRAFIXIAIKRIPTOZASHTITAX', 'MEGWPRMOLHAWABBGOCQNRWBQMQCPHV') : 'DOVERLIVO',
            ('WHYDONTYOU', 'WHYDONTYOU', 'YIEAESVKEZ') : 'KEYWORD',
            ('FIKTOVATAJNA', 'FIKTOVATAINA', 'PDMSVLFQPBQP') : 'PLAYFAIR'
            }

    def testEncrypt(self):
        cipher = cap.PlayfairCipher()

        for d, k in self.CRYPT_DATA.items():
            p, _, expected = d
            res = cipher.encrypt(p, k)
            
            self.assertEqual(expected, res)


    def testDecrypt(self):
        cipher = cap.PlayfairCipher()

        for d, k in self.CRYPT_DATA.items():
            _, expected, c = d
            res = cipher.decrypt(c, k)
            
            self.assertEqual(expected, res)

class VigenereCipherTest(unittest.TestCase):
    
    CRYPT_DATA = {
            ('KRIPTOLOGIJA', ('LIWYUFZXHZXJ', 'LIWYDFTDZWUO') ) : 'BROJ',
            ('DEKRIPTIRANJE', ('YVCMZHOZJVEBZ', 'YVCUMZKQGTVAE')) : 'VRS',
            ('AVTOKLUCH', ('VMLJBDPTZ', 'VMLOFEIMS')) : 'VRS'
            }

    def testEncrypt(self):
        cipher = cap.VigenereCipher()
        cipher_auto_key = cap.VigenereCipher(auto_key=True)
        
        for d, k in self.CRYPT_DATA.items():
            p, (expected_c, expected_c_auto_key) = d
            
            res = cipher.encrypt(p, k)
            res_auto_key = cipher_auto_key.encrypt(p, k)

            self.assertEqual(expected_c, res)
            self.assertEqual(expected_c_auto_key, res_auto_key)

    def testDecrypt(self):
        cipher = cap.VigenereCipher()
        cipher_auto_key = cap.VigenereCipher(auto_key=True)
        
        for d, k in self.CRYPT_DATA.items():
            expected_p, (c, c_auto_key) = d
            
            res = cipher.decrypt(c, k)
            res_auto_key = cipher_auto_key.decrypt(c_auto_key, k)

            self.assertEqual(expected_p, res)
            self.assertEqual(expected_p, res_auto_key)


class HillCipherTest(unittest.TestCase):
    
    CRYPT_DATA = {
            ('CHETVRTOK', 'RSQLQNNDW') : [[17, 17, 5], [21, 18, 21], [2, 2, 19]],
            ('KRIPTOGRAFIJAVOCHETVRTOK', 'FIYYPWBQUGWPLWWRSQLQNNDW') : [[17, 17, 5], [21, 18, 21], [2, 2, 19]],
            ('WEWILLHAVEOURFIRSTTESTNEXTMONDAY', 'MWUULNGXAPAKTTNCHGCBHGILPPQMTFWO') : [[12, 15], [7, 6]]
            }
    
    def testEncrypt(self):
        cipher = cap.HillCipher()

        for d, k in self.CRYPT_DATA.items():
            p, expected = d
            res = cipher.encrypt(p, k)
            
            self.assertEqual(expected, res)


    def testDecrypt(self):
        cipher = cap.HillCipher()

        for d, k in self.CRYPT_DATA.items():
            expected, c = d
            res = cipher.decrypt(c, k)
            
            self.assertEqual(expected, res)


if __name__ == "__main__":
    unittest.main(verbosity=2)
